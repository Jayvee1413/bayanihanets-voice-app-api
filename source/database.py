import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('mysql://jeby:password@127.0.0.1:3306/ryde', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=True,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    load_models()
    Base.metadata.create_all(bind=engine)


def load_models():
    """
        to customize loading of models just override this function
        """

    modules_dir = 'modules'
    models_dir = 'models'

    # get all files in modules directory
    for module in os.listdir(modules_dir):
        # if file is a directory
        if os.path.isdir(os.path.join(modules_dir, module)):
            module_path = os.path.join(modules_dir, '%s/%s' % (module, models_dir))
            for module_file in os.listdir(module_path):
                if len(module_file) >= 3 and module_file[:8] != '__init__' and module_file.endswith('.py'):
                    filename = module_file[:-3]
                    print module_file
                    module_name = module_path.replace('/', '.')
                    print '%s.%s' % (module_name, filename)
                    __import__('%s.%s' % (module_name, filename))

if __name__ == '__main__':
    init_db()