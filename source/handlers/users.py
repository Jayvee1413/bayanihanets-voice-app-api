__author__ = 'jeby'

import ujson
import datetime
import falcon
from tools import encrypt
from tools.route import route
from modules.users.models.main import User
from database import db_session
from tools import redis_lib





@route('/login')
class LoginUserHandler(object):
    """
        Login User Handler
    """
    def on_post(self, req, resp):
        resp.content_type = 'application/json'
        rcon = redis_lib.load('prod_redis')

        email = req.get_param('email')
        password = req.get_param('password')
        print 'PASSWORD'
        print password

        # check if user with email exists
        user = db_session.query(User).filter_by(email=email).first()
        if user and encrypt.verify_password(password, user.password):
            resp.status = falcon.HTTP_401
            # check if token already exists

            token = rcon.get('u:%s' % email)
            if not token:
                # create token
                token = encrypt.create_token()
                rcon.set('u:%s' % email, token)
                rcon.set('t:%s' % token, email)
            resp.body = ujson.dumps({'token': token})
        else:
            raise falcon.HTTPUnauthorized('Unauthorized', 'email/password combination is incorrect')

        return


@route('/users/{user_id}')
class GetUserHandler(object):
    def on_get(self, req, resp, user_id):
        resp.content_type = 'application/json'

        user = db_session.query(User).filter_by(id=user_id).first()
        if user:
            resp.body = ujson.dumps(user.get_json())
        else:
            raise falcon.HTTPNotFound()
        return


@route('/users')
class ListAllUserHandler(object):
    def on_get(self, req, resp):

        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200
        private_columns = ['password']
        users = User.query.all()
        columns = [c for c in User.__table__.columns._data.keys() if c not in private_columns]

        user_list = []
        for user in users:
            data = {}
            for column in columns:
                data[column] = getattr(user, column)
            user_list.append(data)
        resp.body = ujson.dumps({'users': user_list})
        return

    def on_post(self, req, resp):
        try:
            first_name = req.get_param('first_name')
            last_name = req.get_param('last_name')
            email = req.get_param('email')
            password = req.get_param('password')
        except:
            raise falcon.HTTPBadRequest('Missing parameter', 'A parameter is missing')

        new_password = encrypt.encrypt_password(password)
        new_user = User(first_name=first_name, last_name=last_name, email=email, password=new_password)

        print datetime.datetime.now()
        db_session.add(new_user)
        db_session.commit()
        print datetime.datetime.now()
        resp.body = ujson.dumps({'message': 'HELLO 1WORLD!'})
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200
