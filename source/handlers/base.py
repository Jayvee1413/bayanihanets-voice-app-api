__author__ = 'jeby'


class BaseJSONHandler(object):
    """
        Base JSON Handler
    """
    def on_get(self, req, resp):
        resp.body = self.return_body
        resp.content_type = 'application_json'
        resp.status = self.return_status
