__author__ = 'jeby'

from gevent import monkey

monkey.patch_all()

import redis

# Bucket for connected instances
connections = {}


def __connect(config):
    """ Perform retries if the connection fails """
    # Count of retries is defined by MAX_RETRIES
    # host = socket.getaddrinfo(config['host'], 0)[2][4][0]

    r = redis.StrictRedis(host=config['host'], port=config['port'])
    return r


def connect_all(config):
    """ Load multiple sql connections based from the passed configuration dictionary"""
    # Create a MYSQL connection from a Pool connection
    for k, v in config.iteritems():
        connections[k] = __connect(v)
    return connections


def load(key):
    """ Get connected mysql given a key """
    # Key is the name of a mysql db config in settings.yaml
    if key in connections:
        return connections[key]
    return None
