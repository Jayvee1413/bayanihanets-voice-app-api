import hashlib

__author__ = 'jeby'
from passlib.hash import pbkdf2_sha256
import os


def encrypt_password(password):
    hash_p = pbkdf2_sha256.encrypt(password)
    return hash_p


def verify_password(password, hash):
    return pbkdf2_sha256.verify(password, hash)


def create_token():
    return hashlib.sha256(os.urandom(128)).hexdigest()
