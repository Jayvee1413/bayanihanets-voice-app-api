from __future__ import unicode_literals

import gunicorn.app.base
from gunicorn.six import iteritems

from database import db_session
from tools import redis_lib


def number_of_workers():
    # return (multiprocessing.cpu_count() * 2) + 1
    return 2


class StandaloneApplication(gunicorn.app.base.BaseApplication):
    def init(self, parser, opts, args):
        super(StandaloneApplication, self).init(parser, opts, args)

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        redis_config = {'host': '127.0.0.1', 'port': 6379}
        redis_lib.connect_all({'prod_redis': redis_config})
        super(StandaloneApplication, self).__init__()

    def load_config(self):
        config = dict([(key, value) for key, value in iteritems(self.options)
                       if key in self.cfg.settings and value is not None])
        for key, value in iteritems(config):
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


if __name__ == '__main__':
    options = {
        'bind': '%s:%s' % ('0.0.0.0', '80'),
        'workers': number_of_workers(),
        'worker_class': 'gevent'
    }
    from wsgi import api

    StandaloneApplication(api, options).run()
    db_session.remove()
