import glob
import os

__author__ = 'jeby'

import falcon
from tools.route import route


def load_handlers():
    """
    to customize loading of handlers just override this function
    """

    handlers_dir = 'handlers'
    handler_exts = "%s/*.py"

    for f in glob.iglob(handler_exts % handlers_dir):
        # Get the handler file name(w/o extension)
        filename = os.path.basename(f)[:-3]
        # Disregard __init__ and base
        if filename not in ["__init__", "base"]:
            # Get the package's modules
            module_name = '%s.%s' % ('handlers', filename)
            imported = __import__('handlers.%s' % filename, globals(), locals(), -1)


api = application = falcon.API()
load_handlers()
handlers = route.get_routes()
for handler in handlers:
    route = handler[0]
    handler = handler[1]()
    api.add_route(route, handler)
