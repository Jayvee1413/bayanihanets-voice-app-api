.. Ryde documentation master file, created by
   sphinx-quickstart on Tue Oct 13 10:15:11 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ryde's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

