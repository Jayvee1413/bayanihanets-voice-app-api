import ujson

__author__ = 'jeby'
from sqlalchemy import Column, Integer, String
from database import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    first_name = Column(String(100))
    last_name = Column(String(100))
    email = Column(String(120), unique=True)
    password = Column(String(200))

    def __init__(self, first_name=None, last_name=None, email=None, password=None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password

    def __repr__(self):
        return "User: {%s}" % ujson.dumps(
            self.get_json())

    def get_json(self):
        return {'first_name': self.first_name, 'last_name': self.last_name, 'email': self.email, 'id': self.id}
